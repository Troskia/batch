#!/bin/bash
#	Autor:		Andres Julian
#	Date:		20/10/2015
#	Use:		URL content extractor Anilink.tv
#	Version:	0.1

echo "**** Anilink Anime URL Extractor ****"
read -p "URL main Anime: " serie

#Title folder
wget -q  "$serie"
complete=`sed -n 's/.*<title>\(.*\)<\/title>.*/\1/ip;T;q' completa`
complete=${complete%%&*}
mkdir "$complete"
